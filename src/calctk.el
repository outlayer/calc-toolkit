;;;;
;;
;; Elementary functionalities; miro elements
;;
;;;;

;; Defining calc variables
;; Defining compositions for functions
;; Defining key bindings

(defmacro calctk-def--variable (name value)
  "Make «VALUE» recallable by «NAME» inside calc.

Calc's analogue: \\<calc-mode-map>⟦\\[calc-store]⟧.

--- Arguments ---

| NAME   | CalcSymbol    |
| VALUE  | CalcSexp      |
|        | String        |
|--------+---------------|
| RESULT | typeof(VALUE) |

--- Example ---

›  (calctk-def--variable x1 “(-b - sqrt(D)) / 2 a”)

Recallable by ⟦\\[calc-recall]⟧:
(assuming ⟦\\[calc-big-language]⟧ display)

»  │      ___
»  │-b - V D
»  │---------
»  │   2 a
»  │
»  │.
»  │_________
»  │ \\[calc-recall] x1 RET

Can be used as an evaluable variable:

»  │x1
»  │
»  │.
»  │__
»  │ \\[calc-algebraic-entry] x1 RET
»
»    │      ___
»    │-b - V D
»    │---------
»    │   2 a
»    │
»    │.
»    │_________
»    │ \\[calc-evaluate]

In elisp ⟦M-x eval-expression RET var-x1 RET⟧:

›  (/ (- (neg (var b var-b))
›        (calcFunc-sqrt (var D var-D)))
›     (* 2 (var a var-a)))
"
  (calctk-def--variable-f name value))

(defun calctk-def--variable-f (vname value)
  `(setq
    ,(calctk-var-name-symbol vname)
    ',(calctk-ensure-expression-sexp
       value)))

(defmacro calctk-def--keybinding (fname key)
  "Define a key binding KEY to a function ‘calc-FNAME’.
By calc's convention these are the functions operating on
stack. By calctk's narrower convention ‘calc-FNAME’ is the name
of an elisp function pushing «FNAME(a1, a2, …, aN)» algebraic
calc function on stack. Thus FNAME must be a proper calc symbol.

Calc's analogue: \\<calc-mode-map>⟦\\[calc-user-define]⟧.

--- Arguments ---

| FNAME | CalcSymbol |
| KEY   | String     |
"
  (calctk-def--keybinding-f fname key))

(defun calctk-def--keybinding-f (fname key)
  `(define-key
     calc-mode-map
     ,key
     ',(calctk-calc-name-symbol fname)))

(defmacro calctk-def--composition (fname &rest compositions)
  "Define COMPOSITIONS in “Big” mode for function «FNAME».
Associate with symbol ‘calcFunc-FNAME’ a property
‘math-compose-forms’ of value being a sexp representing
compositions for function FNAME in “Big” mode for different
arities. For details on how to define compositions see Info node
‘(calc)Compositions’.

Calc's analogue: \\<calc-mode-map>
⟦\\[calc-big-language]⟧ followed by ⟦\\[calc-user-define-composition]⟧.

--- Arguments ---

| FNAME        | CalcSymbol |                            |
| COMPOSITIONS | Sexp*      | ( (a1 a2 … aN) COMP-EXPR ) |

where:
| a1 a2 … aN | CalcSymbol |
| COMP-EXPR  | String     |
|            | CalcSexp   |

--- Example ---

Compositions for two forms of expected value:

›  (calctk-def--composition
›   expect
›   ;; ∫ x dx
›   ((x)
›    “choriz([string(\\“E[\\”),
›             x,
›             string(\\“]\\”)])”)
›   ;; ∫ a dx
›   ((x a)
›    “choriz([string(\\“E\\”),
›             cvert([cvspace(1), x]),
›                   string(\\“[\\”),
›                   a,
›                   string(\\“]\\”)])”))

In calc:

»  │2:  expect(Z)
»  │1:  expect(Z, Z^2)
»  │    .
»  │__________________
»  │ \\[calc-normal-language]
»
»
»    │2:  E[Z]
»    │
»    │        2
»    │1:  E [Z ]
»    │     Z
»    │
»    │    .
»    │__________
»    │ \\[calc-big-language]
"
  (calctk-def--composition-f fname compositions))

(defun calctk-def--composition-f (fname compositions)
  "
--- Arguments ---

| FNAME        | CalcSymbol |                            |
| COMPOSITIONS | List<T>    | ( (a1 a2 … aN) COMP-EXPR ) |
|--------------+------------+----------------------------|
| RESULT       | Sexp       |                            |

where:
| a1 a2 … aN | CalcSymbol |
| COMP-EXPR  | String     |
|            | CalcSexp   |
"
  (let ((calcFunc-fname (calctk-calcFunc-name-symbol fname)))
    `(put
      ',calcFunc-fname
      'math-compose-forms
      '((big
         ,@(-map
            (lambda (x)
              (apply 'calctk-single-composition x))
            compositions))))))

(defun calctk-single-composition (args raw-body)
  "
--- Arguments ---

| ARGS     | List<CalcSymbol> |
| RAW-BODY | String           |
|          | CalcSexp         |
|----------+------------------|
| RESULT   | Sexp             |
"
  (let* ((body (calctk-ensure-expression-sexp
                raw-body)))
    (list
     (length args)
     'lambda
     args
     (list 'backquote
           (calctk-commafy-form args body)))))

;;;;
;;
;; Elementary functionalities; macro elements
;;
;;;;

;; Defining rules
;; Defining calc-based functions
;; Defining elisp-based functions
;; Defining abstract functions

;;;;
;;
;; Calc rules parsing
;;
;;;;

(defvar calctk-rules-guard (math-read-expr "1 &&& 0 := 0")
  "Impossible to match dummy rule with the sole purpose of
  getting around what seems to be a bug in calc: not recognizing
  a vector as rules set if it's composed only of “import”
  clauses.")

(defun calctk-ensure-string (elem)
  "Convert ELEM sexp nodes into strings.

Arguments:
| ELEM   | T |
|--------+---|
| RESULT | S |

- T  :: Tp | List<T>
- Tp :: Number | String | Symbol
- S  :: String | List<S>

Tp      → String
List<T> → List<S>"
  (cond ((stringp elem) elem)
        ((symbolp elem) (symbol-name elem))
        ((numberp elem) (number-to-string elem))
        ((listp elem) (-map 'calctk-ensure-string elem))
        (t (error "unrecognized type"))))

(defmacro calctk-def--function-rules (rname fname &rest rules)
  "Store in variable «RNAME» rules for evaluating function «FNAME».

Calc's analogue: \\<calc-mode-map>⟦\\[calc-store]⟧ used on rules vector.

--- Arguments ---

| RNAME | CalcSymbol |
|       | String     |
| FNAME | CalcSymbol |
|       | String     |
| RULES | List<Rule> |

- Rule :: As defined by ‘calctk-parse-rule’

--- Detailed description ---

This call makes possible to use in RULES the function pattern
syntax:

›  ((“arg1” “arg2”) “rewr”)

which is translated to

»  FNAME(arg1, arg2) := rewr :: 1

See ‘calctk-parse-rule’ for details. If the RULES don't define
computation on any particular function then ‘calctk-def--rules’
is more appropriate.

For details on how to define rewrite rules see Info node
‘(calc)Rewrite Rules’.
"
  (calctk-def--variable-f
   rname
   (calctk-def--rules-anonymous-f
    rules
    fname)))

(defmacro calctk-def--rules (rname &rest rules)
  "Define a calc variable «RNAME» as a vector of rewrite RULES.

Calc's analogue: \\<calc-mode-map>⟦\\[calc-store]⟧ used on rules vector.

--- Arguments ---

| RNAME | String     |
|       | CalcSymbol |
| RULES | List<Rule> |

- Rule :: as defined by ‘calctk-parse-rule’

--- Detailed description ---

Expand to a setq form assigning a calc sexp representing the
vector of rewrite rules as obtained by calling
‘calctk-parse-rule’ on RULES to the symbol ‘var-RNAME’.

For details on how to define rewrite rules see Info node
‘(calc)Rewrite Rules’.
"
  `(calctk-def--function-rules ,rname nil ,@rules))

(defun calctk-def--rules-anonymous-f (rules &optional func)
  "Return a calc sexp representinc a vector of rewrite RULES.
If any rule is given in the function form it's assumed it
specifies the function FUNC, which in this case must be provided.

--- Arguments ---

| RULES | List<Rule> |

&optional
| FUNC | CalcSymbol | nil |
|      | String     |     |

- Rule :: as defined by ‘calctk-parse-rule’"
  (calctk-list-to-vec
   (let ((calc-rules (--map (calctk-parse-rule it func) rules)))
     (if (or (calctk-elem-is-func (car calc-rules) 'import)
             (calctk-elem-is-func (car calc-rules) 'phase))
         (cons (math-read-expr "1 &&& 0 := 0") calc-rules)
       calc-rules))))

(defun calctk-parse-rule (rule &optional func)
  "Parse RULE into a calc sexp.

--- Arguments ---

|        | Type         | Allowed form                    |
|--------+--------------+---------------------------------|
| RULE   | String       | “patt := rewr :: cond”          |
|        | List<String> | (“rule”)                        |
|        |              | (“patt” “rewr”)                 |
|        |              | (“patt” “rewr” “cond1”)         |
|        |              | (“patt” “rewr” “cond1” “cond2”) |
|        | CalcSymbol   | importedRule                    |
|        | Sexpr        | ((“arg1” “arg2”) “rewr”)        |
|--------+--------------+---------------------------------|
| RESULT | CalcSexpr    |                                 |

&optional
|      | Type       | Default |
|------+------------+---------|
| FUNC | CalcSymbol | nil     |
|      | String     |         |
|      | Null       |         |

--- Detailed description ---

The following translation from elisp object to calc expression is
used:

| Elisp                           | Calc                             |
|---------------------------------+----------------------------------|
| “verbatimString”                | «verbatimString»                 |
| (“rule”)                        | «rule :: 1»                      |
| (“patt” “rewr”)                 | «patt := rewr :: 1»              |
| (“patt” “rewr” “cond”)          | «patt := rewr :: cond»           |
| (“patt” “rewr” “cond1” “cond2”) | «patt := rewr :: cond1 :: cond2» |
| importedRule                    | «import(importedRule)»           |
| ((“arg1” “arg2”) “rewr”)        | «FUNC(arg1, arg2) := rewr :: 1»  |

If RULE is given in the function form, like

›  ((“arg1” “arg2”) “rewr”)

it's assumed to refer to the function FUNC, which in this case
must be provided in the optional argument.
For details on how to define rewrite rules see Info node
‘(calc)Rewrite Rules’.
"
  (cond ((stringp rule) (math-read-expr rule))
        ((symbolp rule) (math-read-expr
                         (concat "import(" (symbol-name rule) ")")))
        ((and (listp rule)
              (> (length rule) 0)
              (-all-p 'stringp rule))
         (math-read-expr
          (cond
           ;; ("unguarded rule")
           ((= (length rule) 1)
            (concat (nth 0 rule) " :: 1"))
           ;; ("pattern" "production")
           ((= (length rule) 2)
            (concat (nth 0 rule) " := " (nth 1 rule) " :: 1"))
           ;; ("pattern" "production" "condition1" "condition2"
           ;; ... "conditionN")
           (t
            (concat (nth 0 rule) " := " (nth 1 rule) " :: "
                    (mapconcat 'identity
                               (cddr rule)
                               " :: "))))))
        ((and (listp rule)
              (> (length rule) 1)
              (listp (car rule))
              (-all? 'stringp (car rule))
              (-all? 'stringp (cdr rule)))
         (if (not func)
             (error "Function pattern given but no function name")
           (-let [(args . rest) rule]
             (calctk-parse-rule
              (cons (concat
                     (calctk-ensure-string func)
                     "("
                     (mapconcat 'identity args ", ")
                     ")")
                    rest)))))
        ((and (listp rule)
              (> (length rule) 0))
         rule)
        (t
         (error "Unrecognized rule type: %s" rule))))

;;;;
;;
;; Defining relations
;;
;;;;

(defmacro calctk-def--relation (name args &rest elements)
  "Define a series of elisp objects specifying calc function «NAME(ARGS)».
Normally you would rather want to use one of the higher-level
macros:
- ‘calctk-def--function’ :: Abstract algebraic function (no
  computation associated, or realized purely through the rewrite
  mechanism).
- ‘calctk-def--function-calc’ :: Algebraic function evaluable to
  a calc expression.
- ‘calctk-def--function-elisp’ :: Algebraic function associated
  with a computation unexpressible in calc and given as an elisp
  algorithm.
They are all expressed as a call to this macro, and only for
their purpose it was created.

--- Arguments ---

| NAME | CalcSymbol       |
| ARGS | List<CalcSymbol> |

&rest (Property list)
| :stack-func-production | CalcSymbol | NAME                     |
|                        | String     |                          |
| :trail-name            | String     | (calctk-trail-name NAME) |
| :key                   | String     | nil                      |
| :algebraic-func-body   | CalcSexp   | nil                      |
|                        | String     |                          |
| :defmath-body          | CalcSexp   | nil                      |
| :composition           | CalcSexp   | nil                      |
|                        | String     |                          |
| :definition-expression | Sexp       | nil                      |

--- Elisp objects defined ---\\<calc-mode-map>

1. If ‘:stack-func-production’ keyword argument is non-nil:
   1. Elisp function ‘calc-NAME’, operating on calc stack by
      taking N arguments from the stack, where N is the length of
      ARGS, and pushing expression «X($N, …, $2, $1)» for X being
      ‘:stack-func-production’.
   2. A property ‘arity’ associated with symbol ‘calc-NAME’
      having value N.
   3. A property ‘calc-user-defn’ associated with symbol
      ‘calc-NAME’ having value t.
2. If ‘:key’ keyword argument is non-nil: Key binding ‘:key’ in
   the ‘calc-mode-map’ invoking function ‘calc-NAME’.
3. If ‘:algebraic-func-body’ keyword argument is non-nil: Elisp
   function ‘calcFunc-NAME’ defining a calculation associated
   with function NAME.
4. If ‘:defmath-body’ keyword argument BODY is non-nil: Calc's
   macro ‘defmath’ is called, as in
   › (defmath NAME ARGS BODY)
5. If ‘:composition’ keyword argument Y is non-nil: A property
   ‘math-compose-forms’ associated with ‘calcFunc-X’ symbol,
   where X is ‘:stack-func-production’ keyword argument if
   non-nil or NAME otherwise, having value of ‘(read-math-expr
   Y)’, and defining the way of displaying this function on the
   stack in “Big” mode (⟦\\[calc-big-language]⟧). The arguments
   list is ARGS.
6. If ‘:definition-expression’ keyword argument is non-nil: A
   property ‘calc-user-defn’ associated with ‘calcFunc-X’ symbol,
   where X is ‘:stack-func-production’ if non-nil or NAME
   otherwise, specifying the calc expression defining this
   function, to be pushed on the stack upon invoking
   ⟦\\[calc-expand-formula]⟧ command on «X(a1, …, aN)» expression
   in calc."
  (let ((stack-func-production (plist-get elements :stack-func-production))
        (trail-name            (plist-get elements :trail-name))
        (key                   (plist-get elements :key))
        (algebraic-func-body   (plist-get elements :algebraic-func-body))
        (defmath-body          (plist-get elements :defmath-body))
        (composition           (plist-get elements :composition))
        (definition-expression (plist-get elements :definition-expression))
        (rest                  (plist-get elements :rest)))
    (let* ((calc-name                (calctk-calc-name-symbol name))
           (calcFunc-name            (calctk-calcFunc-name-symbol name))
           (stack-func-prod          (or stack-func-production
                                         name))
           (calcFunc-stack-func-prod (calctk-calcFunc-name-symbol
                                      stack-func-prod))
           (args-n                   (length args))
           (trail-name-non-nil       (or trail-name
                                         (calctk-trail-name
                                          (symbol-name name))))
           (algebraic-func-body-sexp (calctk-ensure-expression-sexp
                                      algebraic-func-body)))
      `(put 'calc-define ',calc-name
            '(progn
               ,@(-remove
                  'null
                  (append
                   (list
                    ;; crumb: X
                    (if stack-func-production
                        `(defun ,calc-name nil
                           (interactive)
                           (calc-wrapper
                            (calc-enter-result
                             ,args-n
                             ,trail-name-non-nil
                             (cons ',calcFunc-stack-func-prod
                                   (calc-top-list-n ,args-n))))))
                    (if stack-func-production
                        `(put ',calc-name 'arity ,args-n))
                    ;; crumb: U
                    (if stack-func-production
                        `(put ',calc-name 'calc-user-defn 't))
                    (if key
                        (calctk-def--keybinding-f name key))
                    (if algebraic-func-body-sexp
                        `(defun ,calcFunc-name ,args
                           (math-normalize
                            ,algebraic-func-body-sexp)))
                    (if defmath-body
                        `(defmath ,name ,args
                           ,defmath-body))
                    (if composition
                        (calctk-def--composition-f
                         stack-func-prod
                         (list (list args composition))))
                    (if definition-expression
                        `(put
                          ',calcFunc-stack-func-prod
                          'calc-user-defn
                          ',definition-expression)))
                   rest)))))))


(defmacro calctk-def--function-calc (fname
                                     args
                                     calc-definition
                                     &optional
                                     key
                                     composition
                                     &rest
                                     other-elements)
  "Define «FNAME(ARGS)» evaluable to «CALC-DEFINITION».
More precisely, define algebraic function «FNAME(a1, a2, …, aN)»,
for ‘(a1 a2 … aN)’ arguments list given by ARGS, evaluable to
CALC-DEFINITION, which must be a string containing calc expression
or a raw calc sexp. Access this function in calc with KEY,
and display it in “Big” mode using COMPOSITION.

Calc's analogue: \\<calc-mode-map>
⟦\\[calc-user-define-formula]⟧ on «CALC-DEFINITION» on top of the stack
+ optional ⟦\\[calc-user-define-composition]⟧ on «COMPOSITION»
  on top of the stack, preceded by ⟦\\[calc-big-language]⟧.

--- Arguments ---

| FNAME           | CalcSymbol       |
| ARGS            | List<CalcSymbol> |
| CALC-DEFINITION | CalcSexp         |
|                 | String           |

&optional
| KEY         | String | nil |
| COMPOSITION | String | nil |

&rest
| OTHER-ELEMENTS | List<Any> |

OTHER-ELEMENTS are passed as the ‘:rest’ keyword argument to the
‘calctk-def--relation’ by which this macro is expressed.

--- Detailed description ---

Define calc function «FNAME», buildable from the stack

»  │N:    a1
»  │N-1:  a2
»  │…
»  │1:    aN
»  │      .
»  │________

by pressing key sequence KEY

»  │1:  FNAME(a1, a2, …, aN)
»  │    .
»  │________________________

for N being the length of ARGS list.

Display it in “Big” mode (⟦\\[calc-big-language]⟧) according to
COMPOSITION, which must be a string containing calc
expression. For reference on how to define it see Info node
‘(calc)Compositions’. Function's arguments are accessible by
their names given in ARGS.

Upon expansion of «FNAME(a1, a2, …, aN)» (⟦\\[calc-expand-formula]⟧)
substitute it with

»  │1:  CALC-DEFINITION
»  │    .
»  │___________________

CALC-DEFINITION must be a string containing calc expression, or a
raw calc sexp. In each case the arguments are referrable by their
names given in ARGS. The function always evaluates to its
definition upon pressing ⟦\\[calc-evaluate]⟧ as well, regardless
of arguments being symbolic or numeric (suspending evalution in
case of symbolic arguments is currently not supported).

This macro is the close elisp approximation of interactive
⟦\\[calc-user-define-formula]⟧ calc command.

--- Elisp objects defined ---

1. Elisp function ‘calc-FNAME’, operating on calc stack by taking
   N arguments from the stack, where N is the length of ARGS, and
   pushing expression «FNAME($N, …, $2, $1)».
2. A property ‘arity’ associated with symbol ‘calc-FNAME’ having
   value N.
3. A property ‘calc-user-defn’ associated with symbol
   ‘calc-FNAME’ having value t.
4. Elisp function ‘calcFunc-FNAME’ defining a calculation
   associated with function FNAME. Its body is either raw
   CALC-DEFINITION sexpr, or a result of calling
   ‘(math-read-expr CALC-DEFINITION)’ if it's string.
   The arguments list is the same as ARGS.
5. A property ‘calc-user-defn’ associated with ‘calcFunc-FNAME’
   symbol having a value of sexpr representing calc expression
   defining this function, to be pushed on the stack upon
   invoking ⟦\\[calc-expand-formula]⟧ command on
   «FNAME(a1, …, aN)» expression in calc. It is derived from
   CALC-DEFINITION.
6. If KEY is non-nil: Key binding KEY in the
   ‘calc-mode-map’ invoking function ‘calc-FNAME’.
7. If COMPOSITION is non-nil: A property
   ‘math-compose-forms’ associated with ‘calcFunc-FNAME’, having
   value of ‘(read-math-expr COMPOSITION)’, defining the way
   of displaying this function on the stack in “Big” mode
   (⟦\\[calc-big-language]⟧). The arguments list is ARGS.

--- Example ---

A function implementing geometric probability distribution
parameterized by «p» and «k», answering the question of “assuming
that an event occurs with propbability «p» in a single iteration
what's the probability of it occuring in «k» iterations?”

›  (calctk-def--function-calc
›   geomcdf (k p)
›   “1 - (1 - p)^k”
›   “zg”)

Function in action (assuming ⟦\\[calc-no-simplify-mode]⟧ mode):

»  │2:  n      │1:  geomcdf(n, 0.1)    │1:  1 - 0.9^n
»  │1:  0.1    │   .                   │    .
»  │    .      │                       │
»  │_______    │___________________    │_____________
»  │           │ z g                   │ \\[calc-expand-formula]
"
  (let ((calc-definition-sexp (calctk-ensure-expression-sexp
                               calc-definition)))
    `(calctk-def--relation ,fname
                           ,args
                           :stack-func-production
                           ,fname
                           :algebraic-func-body
                           ,(calctk-stack-form-to-algebraic-def
                             args
                             calc-definition-sexp)
                           :definition-expression
                           ,calc-definition-sexp
                           :key
                           ,key
                           :composition
                           ,composition
                           :rest
                           ,other-elements)))

(defmacro calctk-def--function-elisp (fname
                                      args
                                      elisp-definition
                                      &optional
                                      key
                                      composition
                                      &rest
                                      other-elements)
  "Define calc function «FNAME(ARGS)» with verbatim elisp code ELISP-DEFINITION.
More precisely, define algebraic function «FNAME(a1, a2, …, aN)»,
for ‘(a1 a2 … aN)’ arguments list given by ARGS, computing the
resulting value by calling ‘(ELISP-DEFINITION a1 a2 … aN)’ if
ELISP-DEFINITION is Symbol, or by calling an equivalent of
‘(lambda (a1 a2 … aN) ELISP-DEFINITION)’ if it's a sexp.

No direct calc's command analogue.\\<calc-mode-map>
Closest to using ‘defmath’ (see Info node ‘(calc)Defining Functions’)
+ optional ⟦\\[calc-user-define-composition]⟧ on «COMPOSITION»
  on top of the stack, preceded by ⟦\\[calc-big-language]⟧,
+ optional ⟦\\[calc-user-define]⟧ (KEY for stack-based builder).

--- Arguments ---

| FNAME            | CalcSymbol       |
| ARGS             | List<CalcSymbol> |
| ELISP-DEFINITION | Symbol           |
|                  | CalcSexp         |

&optional
| KEY         | String | nil |
| COMPOSITION | String | nil |

OTHER-ELEMENTS are passed as the ‘:rest’ keyword argument to the
‘calctk-def--relation’ by which this macro is expressed.

--- Detailed description ---

Define calc function «FNAME», buildable from the stack

»  │N:    a1
»  │N-1:  a2
»  │…
»  │1:    aN
»  │      .
»  │________

by pressing key sequence KEY,

»  │1:  FNAME(a1, a2, …, aN)
»  │    .
»  │________________________

for N being the length of ARGS list.

It's displayed in “Big” mode (⟦\\[calc-big-language]⟧) according
to calc expression COMPOSITION
(or just as «FNAME(a1, a2, …, aN)» if it's nil).

Upon evaluation (automatic or by pressing ⟦\\[calc-evaluate]⟧ or
⟦\\[calc-alg-evaluate]⟧) the elisp algorithm given by
ELISP-DEFINITION is evaluated and its result is pushed on the
stack. It's up to the user to make sure that the resulting elisp
object defines a correct calc object, that is it is of type
CalcSexp. In the simplest case of a number the correspondence is
1-1, but if, for example, the result shall be a calc symbol «X»
then ELISP-DEFINITION should return sexp ‘(var X var-X)’.

If ELISP-DEFINITION is of type String it is treated as a calc
expression and parsed with calc's built-in ‘math-read-expr’
function into a sexp BODY. The result is used in the definition

›  (defun calcFunc-NAME (a1 a2 … aN)
›    (math-normalize
›     BODY))

where ‹(a1 a2 … aN)› is the list of symbols given by args ARGS.
If it is of type CalcSexp then it is used directly, as in

›  (defun calcFunc-NAME (a1 a2 … aN)
›    (math-normalize
›     ELISP-DEFINITION))

--- Elisp objects defined ---

1. Elisp function ‘calc-FNAME’, operating on calc stack by taking
   N arguments from the stack, where N is the length of ARGS, and
   pushing expression «FNAME($N, …, $2, $1)».
2. A property ‘arity’ associated with symbol ‘calc-FNAME’ having
   value N.
3. A property ‘calc-user-defn’ associated with symbol
   ‘calc-FNAME’ having value t.
4. Elisp function ‘calcFunc-FNAME’ defining a calculation
   associated with function FNAME. Its body is either
   1. A function call ‘(apply 'ELISP-DEFINITION ARGS)’ if
      ELISP-DEFINITION is a Symbol, or
   2. ‘ELISP-DEFINITION’ sexp itself otherwise.
   The arguments list of ‘calcFunc-FNAME’ is ARGS.
5. If KEY is non-nil: Key binding KEY in the
   ‘calc-mode-map’ invoking function ‘calc-FNAME’.
6. If COMPOSITION is non-nil: A property ‘math-compose-forms’
   associated with ‘calcFunc-FNAME’, having value of
   ‘(read-math-expr COMPOSITION)’, defining the way of
   displaying this function on the stack in “Big” mode
   (⟦\\[calc-big-language]⟧). The arguments list is ARGS.

--- Example ---

Function calculating the n-th element 'an' of Fibonacci sequence
for the given initial values 'a0' and 'a1'.

›  (calctk-def--function-elisp
›   fibb
›   (a0 a1 n)
›   (cond ((= n 0) a0)
›         ((= n 1) a1)
›         (t (do ((k   1  (1+ k))
›                 (akp a0 ak)
›                 (ak  a1 (+ ak akp)))
›                ((= k n) ak))))
›   “zf”)

Function in action (assuming ⟦\\[calc-no-simplify-mode]⟧ mode):

»   │3:  10    │1:  fibb(10, 20, 5)    │1:  130
»   │2:  20    │    .                  │    .
»   │1:   5    │                       │
»   │     .    │                       │
»   │______    │___________________    │_______
»   │          │ z f                   │ \\[calc-evaluate]
"
  (let ((algebraic-func-body (if (symbolp elisp-definition)
                                 `(,elisp-definition ,@args)
                               elisp-definition)))
    `(calctk-def--relation ,fname
                           ,args
                           :stack-func-production
                           ,fname
                           :algebraic-func-body
                           ,algebraic-func-body
                           :key
                           ,key
                           :composition
                           ,composition
                           :rest
                           ,other-elements)))


(defmacro calctk-def--function-defmath (fname
                                        args
                                        defmath-definition
                                        &optional
                                        key
                                        composition
                                        &rest
                                        other-elements)
  "Define calc function «FNAME(ARGS)» with ‘defmath’-like elisp code DEFMATH-DEFINITION.
More precisely, define algebraic function «FNAME(a1, a2, …, aN)»,
for ‘(a1 a2 … aN)’ arguments list given by ARGS, computing the
resulting value by calling a function «FNAME» as it was defined
by construct
›  (defmath FNAME (a1 a2 … aN)
›    DEFMATH-DEFINITION)

No direct calc's command analogue.\\<calc-mode-map>
Closest to using ‘defmath’ (see Info node ‘(calc)Defining Functions’)
+ optional ⟦\\[calc-user-define-composition]⟧ on «COMPOSITION»
  on top of the stack, preceded by ⟦\\[calc-big-language]⟧,
+ optional ⟦\\[calc-user-define]⟧ (KEY for stack-based builder).

--- Arguments ---

| FNAME              | CalcSymbol       |
| ARGS               | List<CalcSymbol> |
| DEFMATH-DEFINITION | CalcSexp         |

&optional
| KEY         | String | nil |
| COMPOSITION | String | nil |

OTHER-ELEMENTS are passed as the ‘:rest’ keyword argument to the
‘calctk-def--relation’ by which this macro is expressed.

--- Example ---

Defining an equivalent of the example given in Info node
‘(calc)Defining Functions’, but enhanced with a function builder
bound to a key sequence “zmf”.

›  (calctk-def--function-defmath
›   myfact (n)
›   (if (> n 0)
›       (* n (myfact (1- n)))
›     (if (= n 0)
›         1
›       nil))
›   ”zmf”)

Function in action (assuming ⟦\\[calc-no-simplify-mode]⟧ mode):

»  │7   │myfact(7)   │5040
»  │.   │.           │.
»  │_   │_________   │____
»  │    │ z m f      │ \\[calc-alg-evaluate]
"
  `(calctk-def--relation ,fname
                         ,args
                         :stack-func-production
                         ,fname
                         :defmath-body
                         ,defmath-definition
                         :key
                         ,key
                         :composition
                         ,composition
                         :rest
                         ,other-elements))

(defmacro calctk-def--function (fname
                                args-or-arity
                                key
                                &rest
                                properties)
  "Provide a purely abstract function «FNAME» by defining its builders and rewrite rules.
More precisely, define a stack operating function, accessible by
KEY, which pushes «FNAME($N, …, $2, $1)» on top, where N is
determined from ARGS-OR-ARITY, and an algebraic function
«bFNAME(a1, a2, …, aN)» which evaluates to
«FNAME(a1, a2, …, aN)».

\\<calc-mode-map>No direct calc's command analogue other than simply
using undefined «FNAME(a1, a2, …, aN)» in expressions and
defining all the peripheral objects through:
+ ⟦\\[calc-user-define-formula]⟧ (builder algebraic function «bFNAME»),
+ ⟦\\[calc-user-define]⟧ (KEY for stack-based builder),
+ ⟦\\[calc-user-define-composition]⟧ (‘:composition’ keyword argument),
+ ⟦\\[calc-declare-variable]⟧ (‘:decl-type’ keyword argument),
+ ⟦\\[calc-store]⟧ (‘:rules’ keyword argument).

--- Arguments ---

| FNAME         | CalcSymbol       |
|               | String           |
| ARGS-OR-ARITY | Integer          |
|               | List<CalcSymbol> |
| KEY           | String           |

&rest (Property list)
| :composition | String     | nil |
|              | CalcSexp   |     |
| :decl-type   | String     | nil |
|              | CalcSexp   |     |
| :rules       | List<Rule> | nil |

- Rule :: As defined by ‘calctk-parse-rule’.

--- Detailed description ---

1. Define stack-operating function accessible with a key sequence
   KEY, taking N elements from the stack, for N specified
   directly by ARGS-OR-ARITY if it's a number, or by its length
   if it's a list, and pushing «FNAME($N, …, $1)» on top.

   »  │N:    a1    │1:  FNAME(a1, a2, …, aN)
   »  │N-1:  a2    │    .
   »  │…           │
   »  │1:    aN    │
   »  │      .     │
   »  │________    │__________________________
   »  │            │ KEY

   (Note that «FNAME» function itself is *not* defined.) When in
   “Big” mode display the «FNAME(a1, a2, …, aN)» expression
   according to the value of ‘:composition’ keyword argument, if
   non-nil.

2. Define algebraic function «bFNAME(a1, a2, …, aN)» (“builder”)
   evaluable to «FNAME(a1, a2, …, aN)»

   »  │1:  bFNAME(a1, a2, …, aN)      │1:  FNAME(a1, a2, …, aN)
   »  │    .                          │    .
   »  │___________________________    │__________________________
   »  │                               │ \\[calc-evaluate]

3. Declare the values of «FNAME(a1, a2, …, aN)» to be in the
   domain «X», for X given in ‘:decl-type’ keyword argument, as
   would by done by

   »  │1:  [FNAME(a1, a2, …, aN), X]      │  .
   »  │    .                              │
   »  │_______________________________    │______________
   »  │                                   │ \\[calc-store-concat]
   »                                      | Decls RET

   if X is non-nil.

4. Define variable «ruleFNAME» to be a vector of rewrite rules Y
   given in ‘:rules’ keyword argument, as would by done by

   »  │1:  [ Y ]    │     .
   »  │    .        │
   »  │_________    │__________________
   »  │             │ \\[calc-store-into] ruleFNAME RET

   if Y is non-nil.

--- About abstract functions ---

Purely abstract function is a functions with which no computation
is associated. When pushed on the stack like

»  FNAME(a1, a2, a3)

it remains in the unevaluated form regardless of the
simplification mode being used or the explicit evaluation command
like ⟦\\[calc-evaluate]⟧ or ⟦\\[calc-alg-evaluate]⟧, as well as
of the form of its arguments (symbolic or numeric). As such it's
a great tool to express relations between objects and define what
may be regarded in calc as custom data types.

Since there is no defining body of function the exact naming of
arguments may not be needed and ARGS-OR-ARITY can be either a
list of symbols like ‘(a1 a2 … aN)’ or just a number N. (The only
situation in which there is need for identifying arguments by
their name is when ‘:composition’ keyword argument is provided).

To make this construct available in all possible contexts an
additional layer of computation is introduced in the form of
“builder”: for the abstract function «FNAME» it is a standard
function «bFNAME», with the same arguments list
«(a1, a2, …  aN)», which simply evaluates to
«FNAME(a1, a2, …, aN)». This makes the usage of «FNAME» possible
in vector mapping commands
(⟦\\[calc-map]⟧, ⟦\\[calc-outer-product]⟧, ⟦\\[calc-inner-product]⟧),
which require elisp function of the form ‘calcFunc-FNAME’ to be
defined, which cannot be defined if «FNAME» is to be purely
abstract. Function ‘calcFunc-bFNAME’ can be used in this case
instead.

In practice abstract functions are almost always accompanied by
some simplification rules defined on them, as otherwise their
usefulness would be very limited. In general these can be
specified by ‘calctk-def--rules’, but this macro provides a way
to do it in-place, which, apart from shortening the code,
provides a possibility to use convenient “function form” rules
syntax (see ‘calctk-parse-rule’). The list of rules provided as
‘:rules’ argument are saved in the «ruleFNAME» variable and
available to use on expressions involving constructions
«FNAME(a1, a2, …, aN)» by the use of ⟦\\[calc-rewrite]⟧ or
automatically, by adding «import(ruleFNAME)» to one of the
vectors «EvalRules», «AlgSimpRules», «ExtSimpRules»
(see Info node ‘(calc)Automatic Rewrites’).

--- Elisp objects defined ---

1. Elisp function ‘calc-bFNAME’, operating on calc stack by
   taking N arguments from the stack, where N is either
   ARGS-OR-ARITY if number or the length of ARGS-OR-ARITY if
   list, and pushing expression «FNAME($N, …, $2, $1)».
2. A property ‘arity’ associated with symbol ‘calc-bFNAME’ having
   value N.
3. A property ‘calc-user-defn’ associated with symbol
   ‘calc-bFNAME’ having value t.
4. Key binding KEY in the ‘calc-mode-map’ invoking function
   ‘calc-bFNAME’.
5. Elisp function ‘calcFunc-bFNAME’ of N arguments, representing
   calc function «bFNAME(a1, a2, …, aN)», and evaluating to
   «FNAME(a1, a2, …, aN)».
6. If ‘:composition’ keyword argument X is non-nil: A property
   ‘math-compose-forms’ associated with ‘calcFunc-FNAME’ symbol,
   having value of ‘(read-math-expr X)’, defining the way of
   displaying calc function «FNAME» on the stack in “Big” mode
   (⟦\\[calc-big-language]⟧). The arguments list is ARGS.
7. If ‘:decl-type’ keyword argument Y is non-nil: A setq form
   assigning to symbol ‘var-Decls’ (calc «Decls» variable) the
   value of ‘var-Decls’ expanded by sexp representing
   «[FNAME(a1, a2, …, aN), X]» calc expression. This declares
   function «FNAME» as having values from the domain X.
8. If ‘:rules’ keyword argument Z is non-nil: A ‘setq’ form
   assigning to symbol ‘var-ruleFNAME’
   (calc «ruleFNAME» variable) a vector of rewrite rules Z as
   parsed by function ‘calctk-parse-rule’, which see for the
   exact syntax of Z elements.

--- Example ---

An abstract function «rot3d(a, v1, v2, v3)» representing rotation
in 3D space around a vector «[v1, v2, v3]» by an angle «a».

›  (calctk-def--function
›   rot3d (a v1 v2 v3) “zqR”
›   :composition
›   “choriz([string(\\\"rot3d(\\\"),
›            a,
›            string(\\\", \\\"),
›            clvert([v1,
›                    crule(\\\" \\\"),
›                    cbase(v2),
›                    crule(\\\" \\\"),
›                    v3]),
›            string(\\\")\\\")])”
›   :decl-type “vector”
›   :rules
›   (((“a” “v1” “v2” “v3”)
›     “rot3d(angleNorm(a), v1, v2, v3)”
›     “lor(a < -pi, a >= pi)”)))

Function in action:
(assuming:
- no simplification mode ⟦\\[calc-no-simplify-mode]⟧,
- “Big” display mode ⟦\\[calc-big-language]⟧,
- symbolic mode ⟦\\[calc-symbolic-mode]⟧)

»  │    5       │                w1     │                w1
»  │4:  ─ pi    │          5            │          5
»  │    2       │1:  rot3d(─ pi, w2)    │2:  rot3d(─ pi, w2)
»  │            │          2            │          2
»  │3:  w1      │                w3     │                w3
»  │            │                       │
»  │2:  w2      │    .                  │1:  ruleRot3d
»  │            │                       │
»  │1:  w3      │                       │    .
»  │            │                       │
»  │    .       │                       │
»  │________    │___________________    │___________________
»  |            │ z q R                 │ \\[calc-algebraic-entry]
»                                       │ ruleRot3d RET
»
»
»    │                           w1     │                w1
»    │                    5             │          1
»    │1:  rot3d(angleNorm(─ pi), w2)    │1:  rot3d(─ pi, w2)
»    │                    2             │          2
»    │                           w3     │                w3
»    │                                  │
»    │                                  │    .
»    │    .                             │___________________
»    │______________________________    │ \\[calc-alg-evaluate]
»    │ \\[calc-rewrite]

Function «angleNorm» normalizing the angle «a» to be from the
interval «[-pi, pi)» was assumed to be defined.

If basic simplification mode was used
(⟦\\[calc-basic-simplify-mode]⟧) and «import(ruleRot3d)» was
added to «EvalRules», then the last step would be reached
immediately upon pressing ⟦z q R⟧.

The «ruleRot3d» variable, when inspected, has the form:
(assuming “Normal” display mode ⟦\\[calc-normal-language]⟧)

»  │1:  ruleRot3d    │1:  [rot3d(a, v1, v2, v3)
»  │    .            │       := rot3d(angleNorm(a), v1, v2, v3)
»  │                 │       :: a < -pi || a >= pi]
»  │                 │    .
»  │_____________    │_________________________________________
»  │                 │ \\[calc-evaluate]

The «Decls» variable, assuming it was an empty vector initially,
has the form:

»  │1:  [[ rot3d(a, v1, v2, v3), vector ]]
»  │    .
»  │______________________________________
»  │ \\[calc-recall] Decls RET

Complete list of new objects defined:
1. Elisp objects:
   - ‘calc-brot3d’ function,
   - ‘calcFunc-brot3d’ function,
   - ‘var-ruleRot3d’ variable.
2. Calc objects:
   - «brot3d(a, v1, v2, v3)» evaluable algebraic function,
   - «ruleRot3d» rewrite rules vector."
  (let ((builder-fname (calctk-builder-name-symbol fname)))
    `(calctk-def--function+
      ,builder-fname
      ,fname
      ,args-or-arity
      ,key
      ,@properties)))

(defmacro calctk-def--function+ (builder-name
                                 decl-name
                                 args-or-arity
                                 key
                                 &rest
                                 properties)
  (let ((decl-type   (plist-get properties :decl-type))
        (rules       (plist-get properties :rules))
        (composition (plist-get properties :composition)))
    (let* ((args (if (numberp args-or-arity)
                     (calctk-gen-default-args args-or-arity)
                   args-or-arity))
           (declaration-info
            `(put ',builder-name 'declaration ',decl-name))
           (declaration-type
            (if decl-type
                `(calctk-add-to-Decls
                  ',(cons (calctk-calcFunc-name-symbol decl-name)
                          (-map 'calctk-var-form args))
                  ,decl-type)))
           (rules-definition
            (if rules
                `(calctk-def--function-rules
                  ,(calctk-rule-name-symbol decl-name)
                  ,decl-name
                  ,@rules))))
      `(calctk-def--relation
        ,builder-name
        ,args
        :stack-func-production
        ,decl-name
        :algebraic-func-body
        (list ',(calctk-calcFunc-name-symbol decl-name)
              ,@args)
        :definition-expression
        (list ',(calctk-calcFunc-name-symbol decl-name)
              ,@(-map 'calctk-var-form args))
        :key
        ,key
        :composition
        ,composition
        :rest
        ,(-non-nil (list
                    declaration-info
                    declaration-type
                    rules-definition))))))

(defmacro calctk-def--function-rewrite (fname
                                        key
                                        &optional
                                        rewritesn
                                        &rest
                                        rules)
  "Define calc function «FNAME(expr)» evaluable to «rewrite(expr, RULES)».
Additionaly define stack-operating function pushing «FNAME($1)»
on top, accessible by KEY. The maximum number of iterations for
rewriting is given by REWRITESN.

Calc's analogue: \\<calc-mode-map>
⟦\\[calc-user-define-formula]⟧ on a «rewrite(expr, RULES, REWRITESN)»
expression on top of the stack,
+ ⟦\\[calc-user-define]⟧ (KEY for stack-based builder),
+ ⟦\\[calc-store]⟧ on RULES on top of the stack (if given).

--- Arguments ---

| FNAME | CalcSymbol |
|       | String     |
| KEY   | String     |

&optional
| REWRITESN | Integer | nil |
|           | Null    |     |

&rest
| RULES | List<Rule> | nil |

- Rule :: As defined by ‘calctk-parse-rule’.

--- Detailed description ---

Define calc function «FNAME» of 1 argument, buildable from the
stack

»  │1:  expr
»  │    .
»  │________

by pressing key sequence KEY

»  │1:  FNAME(expr)
»  │    .
»  │_______________

and evaluable to

»  │1:  rewrite(expr, ruleFNAME, REWRITESN)
»  │    .
»  │_______________________________________

where «ruleFNAME» is a newly defined calc variable containing
rewrite rules specified in RULES if its non-nil, or left in
symbolic form otherwise.

This type of function is suited for transformations best
expressible through rewrite mechanism, but for which retaining
the on-demand nature of typical algebraic functions, as opposed
to background automatic simplifications, is desired. The
«FNAME(expr)» construct can be used in algebraic expressions and
KEY can be used while operating on stack to invoke the rewriting.

Essentially the call to this macro is equal to the call

›  (calctk-def--function-calc
›   FNAME (expr)
›   “rewrite(expr, RULES, REWRITESN)”
›   KEY)

(see ‘calctk-def--function-calc’) with REWRITESN omitted if nil,
preceded by a call

›  (calctk-def--rules ruleFNAME RULES)

(see ‘calctk-def--rules’) saving RULES in the variable
«ruleFNAME» (unless RULES is nil).

--- Example ---

A function converting equality of two vectors into the vector of
equalities.

›  (calctk-def--function-rewrite
›   eqDistrib
›   “zd”
›   300
›   (“plain([A] = [B])” “[A = B]”)
›   (“plain(cons(H1,T1) = cons(H2,T2))”
›    “cons(H1 = H2, T1 = T2)”))

The defined rule:

»  │1:  ruleEqDistrib    │1:  [[A] = [B] := [A = B] :: 1,
»  │    .                │     cons(H1, T1) = cons(H2, T2)
»  │                     │       := cons(H1 = H2, T1 = T2) :: 1]
»  │                     │ .
»  │_________________    │______________________________________
»  │                     │ \\[calc-evaluate]

Function in action (assuming ⟦\\[calc-no-simplify-mode]⟧ mode):

»  │2:  [a, b, c]               │1:  [a, b, c]
»  │1:  [-1.48, -0.89, 0.43]    │      = [-1.48, -0.89, 0.43]
»  │    .                       │    .
»  │________________________    │____________________________
»  │                            │ \\[calc-equal-to]
»
»
»    │1:  eqDistrib([a, b, c]         │1:  rewrite([a, b, c]
»    │      = [-1.48, -0.89, 0.43])   │      = [-1.48, -0.89, 0.43],
»    │    .                           │            ruleEqDistrib, 300)
»    │                                │    .
»    │_____________________________   │_______________________________
»    │ z d                            │ C-u - 1 \\[calc-alg-evaluate]
»
»
»    │1:  [a = -1.48, b = -0.89,
»    │     c = 0.43]
»    │    .
»    │__________________________
»    │ \\[calc-alg-evaluate]

Complete list of objects defined:
1. Elisp objects:
   - ‘calc-eqDistrib’ function
   - ‘calcFunc-eqDistrib’ function
   - ‘var-ruleEqDistrib’ variable
2. Calc objects:
   - «eqDistrib(x)» evaluable algebraic function
   - «ruleEqDistrib» variable containing vector of rewrite rules"
  (let* ((rule-name (calctk-rule-name-symbol fname))
         (rule-name-var-form (calctk-var-form rule-name))
         (rules-definitions
          (if rules
              `((calctk-def--rules ,rule-name ,@rules))))
         (n (if rewritesn (list rewritesn))))
    `(progn
       ,@rules-definitions
       (calctk-def--function-calc
        ,fname
        (expr)
        (calcFunc-rewrite (var expr var-expr) ,rule-name-var-form ,@n)
        ,key))))

;;;;
;;
;; Normalization of binary operators
;;
;;;;

(defmacro calctk-def--rule-binop-assoc-norm (oper left-assoc)
  "Define rules normalizing associative binary operator «OPER».
“Normalizing” means converting arbitrary binary operator tree
into a linearized form while preserving the order of operands.
Generate left-associating rules and save them in
«ruleOPERLeftAssocNorm» if LEFT-ASSOC is non-nil, or
right-associating rules saved in «ruleOPERRightAssocNorm»
otherwise.

--- Arguments ---

| OPER       | CalcSymbol |
| LEFT-ASSOC | Boolean    |

--- Example ---\\<calc-mode-map>

Calling

›  (calctk-def--rule-binop-assoc-norm land t)

would define variable «ruleLandLeftAssocNorm» which, when
applied  (⟦\\[calc-rewrite]⟧) to the expression:

»  │land(land(x1, x2), land(land(x3, land(x4, x5)), x6))
»  │.
»  │____________________________________________________
»  │ \\[calc-unformatted-language]
»
»    │x1 && x2 && (x3 && (x4 && x5) && x6)
»    │.
»    │____________________________________
»    │ \\[calc-normal-language]

would produce:

»  │land(land(land(land(land(x1, x2), x3), x4), x5), x6)
»  │.
»  │____________________________________________________
»  │ \\[calc-unformatted-language]
»
»    │x1 && x2 && x3 && x4 && x5 && x6
»    │.
»    │________________________________
»    │ \\[calc-normal-language]

If the following call was used:

›  (calctk-def--rule-binop-assoc-norm land nil)

then the «ruleLandRightAssocNorm» would be defined, producing

»  │land(x1, land(x2, land(x3, land(x4, land(x5, x6)))))
»  │.
»  │____________________________________________________
»  │ \\[calc-unformatted-language]
»
»    │x1 && (x2 && (x3 && (x4 && (x5 && x6))))
»    │.
»    │________________________________________
»    │ \\[calc-normal-language]

--- Generated rules ---

1. If LEFT-ASSOC is non-nil:

   »  plain(OPER(plain(OPER(a, b)),
   »             plain(OPER(c, d))))
   »    := OPER(OPER(OPER(a, b), c), d)
   »
   »  plain(OPER(a
   »             &&& !!! plain(OPER(x, y)),
   »             plain(OPER(b, c))))
   »    := OPER(OPER(a, b), c)

2. If LEFT-ASSOC is nil:

   »  plain(OPER(plain(OPER(a, b)),
   »             plain(OPER(c, d))))
   »     := OPER(a, OPER(b, OPER(c, d)))
   »
   »  plain(OPER(plain(OPER(a, b)),
   »             c &&& !!! plain(OPER(x, y))))
   »     := OPER(a, OPER(b, c))
"
  (let (
        ;; var-ruleLandLeftAssocNorm
        (rname (calctk-rule-name
                (calctk-assoc-name
                 oper
                 (if left-assoc "left" "right"))))
        ;; calcFunc-land
        (calcFunc-oper (calctk-calcFunc-name-symbol
                        oper)))
    `(calctk-def--variable
      ,rname
      (vec
       ,@(if left-assoc
             (calctk-def--rule-binop-lassoc-norm
              calcFunc-oper)
           (calctk-def--rule-binop-rassoc-norm
            calcFunc-oper))))))

(defun calctk-def--rule-binop-lassoc-norm (calcFunc-oper)
  (calctk-def--rule-binop-lassoc-norm+
   calcFunc-oper
   calcFunc-oper
   calcFunc-oper))

(defun calctk-def--rule-binop-lassoc-norm+ (calcFunc-oper1
                                            calcFunc-oper2
                                            calcFunc-oper3)
  `(
    (calcFunc-condition
     (calcFunc-assign
      (calcFunc-plain
       (,calcFunc-oper2 (calcFunc-plain
                         (,calcFunc-oper1 (var a var-a)
                                          (var b var-b)))
                        (calcFunc-plain
                         (,calcFunc-oper3 (var c var-c)
                                          (var d var-d)))))
      (,calcFunc-oper3 (,calcFunc-oper2 (,calcFunc-oper1 (var a var-a)
                                                         (var b var-b))
                                        (var c var-c))
                       (var d var-d)))
     1)
    (calcFunc-condition
     (calcFunc-assign
      (calcFunc-plain
       (,calcFunc-oper1 (calcFunc-pand
                         (var a var-a)
                         (calcFunc-pnot
                          (calcFunc-plain
                           (,calcFunc-oper3 (var x var-x)
                                            (var y var-y)))))
                        (calcFunc-plain
                         (,calcFunc-oper2 (var b var-b)
                                          (var c var-c)))))
      (,calcFunc-oper2 (,calcFunc-oper1 (var a var-a)
                                        (var b var-b))
                       (var c var-c)))
     1)))

(defun calctk-def--rule-binop-rassoc-norm (calcFunc-oper)
  (calctk-def--rule-binop-rassoc-norm+
   calcFunc-oper
   calcFunc-oper
   calcFunc-oper))

(defun calctk-def--rule-binop-rassoc-norm+ (calcFunc-oper1
                                            calcFunc-oper2
                                            calcFunc-oper3)
  `(
    (calcFunc-condition
     (calcFunc-assign
      (calcFunc-plain
       (,calcFunc-oper2 (calcFunc-plain
                         (,calcFunc-oper1 (var a var-a)
                                          (var b var-b)))
                        (calcFunc-plain
                         (,calcFunc-oper3 (var c var-c)
                                          (var d var-d)))))
      (,calcFunc-oper1 (var a var-a)
                       (,calcFunc-oper2 (var b var-b)
                                        (,calcFunc-oper3 (var c var-c)
                                                         (var d var-d)))))
     1)
    (calcFunc-condition
     (calcFunc-assign
      (calcFunc-plain
       (,calcFunc-oper2 (calcFunc-plain
                         (,calcFunc-oper1 (var a var-a)
                                          (var b var-b)))
                        (calcFunc-pand
                         (var c var-c)
                         (calcFunc-pnot
                          (calcFunc-plain
                           (,calcFunc-oper3 (var x var-x)
                                            (var y var-y)))))))
      (,calcFunc-oper1 (var a var-a)
                       (,calcFunc-oper2 (var b var-b)
                                        (var c var-c))))
     1)))

;;;;
;;
;; Conversion  binary operator -> vector
;;
;;;;

(defmacro calctk-def--rule-binop-to-vec (oper left-assoc)
  "Define rules variable «ruleOPERToVec» transforming chain of OPER expressions into a vector.

--- Arguments ---

| OPER       | CalcSymbol |
| LEFT-ASSOC | Boolean    |

--- Detailed description ---

Assign to variable «ruleOPERToVec» a vector of rewrite rules
changing a chain of expressions joined with binary operator
«OPER» into a vector.

To be properly transformed the chain must be in either left- or
right-associated form. For example, for «OPER = land» the
expression involving «a1, a2, a3, a4» must be either

»  ((a1 && a2) && a3) && a4

or

»  a1 && (a2 && (a3 && a4))

(For changing an arbitrary binary operator tree into one of
those forms see ‘calctk-def--rule-binop-assoc-norm’). If
LEFT-ASSOC is non-nil generate rules for the first form,
otherwise for the second, and save them in the variable
«ruleLandToVec». The result of applying either of them will be

»  [a1, a2, a3, a4]

--- Generated rules ---

1. If LEFT-ASSOC is non-nil:

   »  plain(OPER(a
   »             &&& !!! cons(h, t)
   »             &&& !!! OPER(x, y),
   »             b))                  := [a, b]
   »  plain(OPER(cons(h, t), a))      := rcons(cons(h, t), a)

2. If LEFT-ASSOC is nil:

   »  plain(OPER(a,
   »             b
   »             &&& !!! cons(h, t)
   »             &&& !!! OPER(x, y))) := [a, b],
   »  plain(OPER(a, cons(h, t)))      := cons(a, cons(h, t))
"
  (let* ((calcFunc-oper (calctk-calcFunc-name-symbol
                         oper))
         (var-rule (calctk-var-name-symbol
                    (calctk-rule-name
                     (calctk-conversion-name
                      oper
                      "vec")))))
    (if left-assoc
        `(calctk-def--lassoc-oper-to-vec ,var-rule ,calcFunc-oper)
      `(calctk-def--rassoc-oper-to-vec ,var-rule ,calcFunc-oper))))

(defmacro calctk-def--lassoc-oper-to-vec (var-rule calcFunc-op)
  `(setq ,var-rule
         '(vec
           (calcFunc-assign
            (calcFunc-plain (,calcFunc-op
                             (calcFunc-pand
                              (calcFunc-pand
                               (var a var-a)
                               (calcFunc-pnot
                                (calcFunc-cons
                                 (var h var-h)
                                 (var t var-t))))
                              (calcFunc-pnot
                               (,calcFunc-op
                                (var x var-x)
                                (var y var-y))))
                             (var b var-b)))
            (vec
             (var a var-a)
             (var b var-b)))
           (calcFunc-assign
            (calcFunc-plain (,calcFunc-op
                             (calcFunc-cons
                              (var h var-h)
                              (var t var-t))
                             (var a var-a)))
            (calcFunc-rcons
             (calcFunc-cons
              (var h var-h)
              (var t var-t))
             (var a var-a))))))

(defmacro calctk-def--rassoc-oper-to-vec (var-rule calcFunc-op)
  `(setq ,var-rule
         '(vec
           (calcFunc-assign
            (calcFunc-plain
             (,calcFunc-op (var a var-a)
                           (calcFunc-pand
                            (calcFunc-pand
                             (var b var-b)
                             (calcFunc-pnot
                              (calcFunc-cons
                               (var h var-h)
                               (var t var-t))))
                            (calcFunc-pnot
                             (,calcFunc-op (var x var-x)
                                           (var y var-y))))))
            (vec
             (var a var-a)
             (var b var-b)))
           (calcFunc-assign
            (calcFunc-plain
             (,calcFunc-op (var a var-a)
                           (calcFunc-cons
                            (var h var-h)
                            (var t var-t))))
            (calcFunc-cons
             (var a var-a)
             (calcFunc-cons
              (var h var-h)
              (var t var-t)))))))

(defmacro calctk-def--rule-binop-to-vec--function-rewrite
    (oper key left-assoc &optional n)
  "Define a function «OPERToVec» transforming chain of «OPER» binary operators into a vector.

--- Arguments ---

| OPER       | CalcSymbol |
| KEY        | String     |
| LEFT-ASSOC | Boolean    |

&optional
| N | Integer | nil |

--- Detailed description ---

1. Define the variable «ruleOPERToVec» exctly as a call

   ›  (‘calctk-def--rule-binop-to-vec’ OPER LEFT-ASSOC)

   would do, which see.

2. Define a function «OPERToVec(expr)», accessible via KEY, which
   uses «ruleOPERToVec» to rewrite «expr» changing all «OPER»
   binary operator chains into vectors. This is done by the use
   of «rewrite» built-in function, to which N is passed as the
   maximum number of iterations.

--- Example ---\\<calc-mode-map>

›  (calctk-def--rule-binop-to-vec--function-rewrite
›    lor “zlv” t 100)

Function in action (assuming ⟦\\[calc-no-simplify-mode]⟧ mode):

»  │1:  p && (q || r || s)    │1:  lorToVec(p && (q || r || s)
»  │      && (t || v)         │      && (t || v))
»  │    .                     │    .
»  │______________________    │_______________________________
»  │                          │ z l v
»
»
»    │1:  rewrite(p && (q || r || s) && (t || v),
»    │            ruleLorToVec, 100)
»    │    .
»    │___________________________________________
»    │ C-u - 1 \\[calc-expand-formula]
»
»
»    │1:  p && [q, r, s] && [t, v]
»    │    .
»    │____________________________
»    │ \\[calc-evaluate]

Complete list of objects defined:
1. Elisp objects:
   - ‘var-ruleLorToVec’ variable
   - ‘calc-lorToVec’ function
   - ‘calcFunc-lorToVec’ function
2. Calc objects:
   - «ruleLorToVec» variable of rewrite rules
   - «lorToVec(x)» calc-expressible algebraic function"
  `(progn
     (calctk-def--rule-binop-to-vec
      ,oper
      ,left-assoc)
     (calctk-def--function-rewrite
      ,(calctk-conversion-name-symbol oper "vec")
      ,key
      ,n)))

(defun calctk-stack-form-to-algebraic-def (args stack-def-form)
  "Convert CalcSexp object an elisp form usable as a body of an elisp function.

--- Arguments ---

| ARGS           | List<CalcSymbol> |
| STACK-DEF-FORM | CalcSexp         |
"
  (cons 'backquote
        (list
         (calctk-commafy-form
          args
          stack-def-form))))

;;;;
;;
;; Conversion: vector -> binary operator
;;
;;;;

(defmacro calctk-def--rule-vec-to-binop (oper)
  "Define rules converting vector to a chain of elements joined by OPER.
Save them in the variable «ruleVecToOPER». The chain of operands
is right-associated.

--- Arguments ---

| OPER | CalcSymbol |

--- Example ---\\<calc-mode-map>

› (calctk-def--rule-vec-to-binop min)

In calc:

»  │2:  [a, b, c]       │1:  min(a, min(b, c))
»  │1:  ruleVecToMin    │    .
»  │    .               │_____________________
»  │________________    │ \\[calc-rewrite] RET
»  │

»  │2:  [x]             │1:  x
»  │1:  ruleVecToMin    │    .
»  │    .               │_____
»  │________________    │ \\[calc-rewrite] RET
»  │

»  │2:  []              │1:  []
»  │1:  ruleVecToMin    │    .
»  │    .               │______
»  │________________    │ \\[calc-rewrite] RET
»  │

--- Generated rules ---

»  [a, b]     := OPER(a, b)
»  [a]        := a
»  cons(h, t) := OPER(h, t)
"
  (let ((rule (calctk-rule-name-symbol
               (calctk-conversion-name-str
                "vec"
                oper)))
        (calcFunc-oper (calctk-calcFunc-name-symbol
                        oper)))
    `(calctk-def--variable ,rule
                           (vec
                            (calcFunc-assign
                             (vec
                              (var a var-a)
                              (var b var-b))
                             (,calcFunc-oper
                              (var a var-a)
                              (var b var-b)))
                            (calcFunc-assign
                             (vec (var a var-a))
                             (var a var-a))
                            (calcFunc-assign
                             (calcFunc-cons
                              (var h var-h)
                              (var t var-t))
                             (,calcFunc-oper
                              (var h var-h)
                              (var t var-t)))))))

(defmacro calctk-def--rule-vec-to-binop--function-rewrite
    (oper key &optional n)
  "Define function «vecToOPER» joining vectors' elemets with «OPER».

--- Arguments ---

| OPER | CalcSymbol |
| KEY  | String     |

&optional
| N | Integer | nil |

1. Define the variable «ruleVecToOPER» exactly as a call

   ›  (‘calctk-def--rule-vec-to-binop’ OPER)

   would do, which see.

2. Define a function «vecToOPER(expr)», accessible via KEY, which
   uses «ruleVecToOPER» to rewrite «expr» changing all vectors
   into sequences of elements joined with binary operator «OPER»,
   right-associated. This is done by the use of «rewrite»
   built-in function, to which N is passed as the maximum number
   of iterations.

--- Example ---\\<calc-mode-map>

›  (calctk-def--rule-vec-to-binop--function-rewrite
›   lor “zL” 100)

Function in action (assuming ⟦\\[calc-no-simplify-mode]⟧ mode):

»  │[ [ u11, u12 ]      │vecToLor([ [ u11, u12 ]  )
»  │  [ u21, u22 ] ]    │           [ u21, u22 ] ]
»  │.                   │.
»  │________________    │__________________________
»  │                    │ z L
»
»  │rewrite([ [ u11, u12 ]  , ruleVecToLor, 100)
»  │          [ u21, u22 ] ]
»  │.
»  │____________________________________________
»  │ C-u - 1 \\[calc-expand-formula]
»
»  │u11 || u12 || (u21 || u22)
»  │.
»  │__________________________
»  │ \\[calc-alg-evaluate]
»
»  │lor(lor(u11, u12), lor(u21, u22))
»  │.
»  │_________________________________
»  │ \\[calc-unformatted-language]

Complete list of objects defined:
1. Elisp objects:
   - ‘var-ruleVecToLor’ variable
   - ‘calc-vecToLor’ function
   - ‘calcFunc-vecToLor’ function
2. Calc objects:
   - «ruleVecToLor» variable
   - «vecToLor(x)» algebraic function
"
  `(progn
     (calctk-def--rule-vec-to-binop ,oper)
     (calctk-def--function-rewrite
      ,(calctk-conversion-name-symbol "vec" oper)
      ,key
      ,n)))

;;;;
;;
;; Conversion between binary operators
;;
;;;;

(defmacro calctk-def--function-rule-binop-to-binop (key rewritesn oper1 oper2)
  "Define rules variable «ruleOPER1ToOPER2» converting OPER1 to OPER2.
Define also a function, accessible by KEY, evaluable to
«rewrite($1, ruleOPER1ToOPER2, REWRITESN)».

--- Arguments ---

| KEY       | String     |
| REWRITESN | Integer    |
| OPER1     | CalcSymbol |
| OPER2     | CalcSymbol |

--- Example ---\\<calc-mode-map>

›  (calctk-def--function-rule-binop-to-binop
›   ”zt” 100 min max)

In calc:

»  │ruleMinToMax   │[plain(min(a, b)) := max(a, b) :: 1]
»  │.              │.
»  │____________   │
»  │               │____________________________________
»                  │ \\[calc-evaluate]

»  │min(a, 1 / a)   │max(a, 1 / a)
»  │.               │.
»  │_____________   │_____________
»  │                │ z t
"
  (let ((func-name (calctk-conversion-name oper1 oper2)))
    `(progn
       (calctk-def--rule-binop-to-binop ,oper1 ,oper2)
       (calctk-def--function-rewrite ,func-name ,key ,rewritesn))))

(defmacro calctk-def--rule-binop-to-binop (oper1 oper2)
  "Define rules variable «ruleOP1ToOP2» converting «OP1» into «OP2».

--- Arguments ---

| OPER1 | CalcSymbol |
| OPER2 | CalcSymbol |

--- Generated rules ---

»  plain(OP1(a, b)) := OP2(a, b)
"
  (let ((rule (calctk-rule-name-symbol
               (calctk-conversion-name oper1 oper2)))
        (calcFunc-oper1 (calctk-calcFunc-name-symbol oper1))
        (calcFunc-oper2 (calctk-calcFunc-name-symbol oper2)))
    `(calctk-def--variable ,rule
                           (vec
                            (calcFunc-condition
                             (calcFunc-assign
                              (calcFunc-plain
                               (,calcFunc-oper1
                                (var a var-a)
                                (var b var-b)))
                              (,calcFunc-oper2
                               (var a var-a)
                               (var b var-b)))
                             1)))))

;;;;
;;
;; Generating operator guarding rules
;;
;;;;

(defmacro calctk-def--rules-guarded (rname
                                     opers
                                     phase-start
                                     &rest
                                     rules)
  "Define in variable «RNAME» a set of a-cyclic rules based on cyclical RULES.
Achieve that by composing «RNAME» rules of two phases
1. All the rules given in RULES, but with every occurence of
   function «oper» appearing in OPERS on the right-hand side of
   «:=» changed into an alternative, unique name.
2. Return to the original operator names.
Phases numbering starts with PHASE-START.

--- Arguments ---

| RNAME       | CalcSymbol       |
| OPERS       | List<CalcSymbol> |
| PHASE-START | Integer          |
| RULES       | List<Rule>       |

- Rule :: as defined by ‘calctk-parse-rule’

--- Detailed description ---

Cyclical rules are those which produce on the right-hand side of
operator «:=» something that matches its left-hand side without
any reduction of structure terminating the rewriting mechanism
eventually, for example “inequality swapping” rule:

»  A < B := B < A

To make it actually achieving the goal of swapping the sides of a
given inequality and not get trapped in an infinite loop it must
be split into two parts, like:

»  phase(1)
»  A < B := tempLt(B, A)
»  phase(2)
»  tempLt(B, A) := B < A

This macro does exactly that.

More generally, a cyclical rules set is any rules set for which
there exists an expression which, has this rules set been applied
to, would lock the rewrite mechanism in an infinite loop. The
loop may span multiple rules, like in:

»  [ f(X) := g(X),
»    g(X) := f(X) ]

The macro will work in the general case as well, breaking the
cycle upon the occurence in an expression of any operator given
in OPERS, which may be thought of as the set of terminating
operators, rewritten exactly once.

--- Example ---\\<calc-mode-map>

A rule swapping the order of equality and inequality relations,
preserving their meaning.

›  (calctk-def--rules-guarded
›   ruleSwapSides (eq lt gt leq geq) 1
›   (“plain(A = B)” “B = A”)
›   (“A < B” “B > A”)
›   (“A > B” “B < A”)
›   (“A <= B” “B >= A”)
›   (“A >= B” “B <= A”))

In calc:

»  │ruleSwapSides
»  │.
»  │_____________
»  │
»
»    │[phase(1),
»    │ plain(A = B) := eq297618(B, A)
»    │   :: 1,
»    │ A < B := gt276738(B, A) :: 1,
»    │ A > B := lt814379(B, A) :: 1,
»    │ A <= B := geq230405(B, A) :: 1,
»    │ A >= B := leq672059(B, A) :: 1,
»    │ phase(2),
»    │ plain(eq297618(arg1, arg2))
»    │   := arg1 = arg2 :: 1,
»    │ plain(lt814379(arg1, arg2))
»    │   := arg1 < arg2 :: 1,
»    │ plain(gt276738(arg1, arg2))
»    │   := arg1 > arg2 :: 1,
»    │ plain(leq672059(arg1, arg2))
»    │   := arg1 <= arg2 :: 1,
»    │ plain(geq230405(arg1, arg2))
»    │   := arg1 >= arg2 :: 1]
»    │.
»    │_________________________________
»    │ \\[calc-evaluate]
"
  (calctk-def--variable-f
   rname
   (calctk-gen--rules-guarded-f
    (calctk-def--rules-anonymous-f rules)
    opers
    phase-start)))

(defun calctk-gen--rules-guarded-f (cyclical-rules
                                    opers
                                    &optional
                                    phase-start)
  (let* ((calcFunc-opers-alt-names
          (-zip-pair
           (-map 'calctk-calcFunc-name-symbol opers)
           (-map
            'calctk-calcFunc-name-symbol
            (-map 'calctk-gen-alt-symbol opers))))
         (calcFunc-opers-alt-names-arities
          (--map (cons it (calctk-arities cyclical-rules (car it)))
                 calcFunc-opers-alt-names))
         (phase-start-ext (or phase-start 1)))
    `(vec
      (calcFunc-phase ,phase-start-ext)
      ,@(cdr (calctk-guard-opers-righthand-assign
              cyclical-rules
              calcFunc-opers-alt-names))
      (calcFunc-phase ,(+ phase-start-ext 1))
      ,@(calctk-gen-come-back-rules
         calcFunc-opers-alt-names-arities))))

(defun calctk-arities (form cF-oper)
  "Return the list of arities of the operator CF-OPER which it
assumes in the FORM

--- Arguments ---

| FORM    | CalcSexp      | Any calc expression          |
| CF-OPER | Symbol        | ‘calcFunc-X’ form for X      |
|         |               | of type CalcSymbol           |
|---------+---------------+------------------------------|
| RESULT  | List<Integer> | List of all arities function |
|         |               | CF-OPER occurs with withing  |
|         |               | expression FORM              |
"
  (-distinct
   (-remove
    'null
    (-flatten
     (calctk-arities+ form cF-oper)))))

(defun calctk-arities+ (form cF-oper)
  (cond ((calctk-is-cF-func form cF-oper)
         (cons (length (cdr form))
               (calctk-arities (cdr form) cF-oper)))
        ((listp form) (--map (calctk-arities it cF-oper) form))
        (t nil)))

(defun calctk-gen-come-back-rules (calcFunc-opers-alt-names-arities)
  "calcFunc-opers-alt-names-arities:
( .. ( (oper . oper739482) . (2 3) ) .. )"
  (apply 'append
         (-map (lambda (opertrans)
                 (let ((cF-oper (caar opertrans))
                       (cF-oper-guard (cdar opertrans))
                       (arities (cdr opertrans)))
                   (--map
                    (calctk-gen-oper-change-rule
                     cF-oper-guard
                     cF-oper
                     it)
                    arities)))
               calcFunc-opers-alt-names-arities)))


(defun calctk-gen-oper-change-rule (cF-oper-guard cF-oper arity)
  (let ((args (-map 'calctk-var-form (calctk-gen-default-args arity))))
    `(calcFunc-condition
      (calcFunc-assign
       (calcFunc-plain
        (,cF-oper-guard ,@args))
       (,cF-oper ,@args))
      1)))

;;;;
;;
;; Vector spaces
;;
;;;;

(defmacro calctk-def--rules-func-linear-expand (fname
                                                &optional
                                                n
                                                j
                                                rule-name
                                                arg1-symbol
                                                arg2-symbol
                                                scalar-symbol)
  "Assign to «ruleFNAMELinExpand» the vector space linearity rules.
Generate expanding linearity rules for function «FNAME» of N
arguments, linear on Jth argument, as in
«FNAME(s, α x + y) = α FNAME(s, x) + FNAME(s, y)»
(for N = 2, J = 2, α: scalar, x,y: vectors).

--- Arguments ---

| FNAME | CalcSymbol |

&optional
| N             | Integer    | 1                  |
| J             | Integer    | 1                  |
| RULE-NAME     | CalcSymbol | ruleFNAMELinExpand |
| ARG1-SYMBOL   | CalcSymbol | X                  |
| ARG2-SYMBOL   | CalcSymbol | Y                  |
| SCALAR-SYMBOL | CalcSymbol | a                  |

--- Example ---\\<calc-mode-map>

›  (calctk-def--rules-func-linear-expand foo 3 2)
›  (calctk-add-to-Decls ”g” ”[scalar, real]”)

In calc:

»  │2:  foo(a + b, g x + h y, 3)    │1:  g foo(a + b, x, 3)
»  │1:  ruleFooLinExpand            │      + foo(a + b, h y, 3)
»  │    .                           │    .
»  │____________________________    │__________________________
»  │                                │ \\[calc-rewrite]

Notice the lack of expansion of «foo(a + b, h y, 3)» into
«h foo(a + b, y, 3)», as opposed to «foo(a + b, g x, 3)» being
expanded to «g foo(a + b, x, 3)». This is because only «g» was
declared as real scalar (see ‘calctk-add-to-Decls’ for
details). Also «a + b» wasn't expanded, as «foo» is linear only
on the second argument.

--- Generated rules ---

(N = 3, J = 2 used for clarity, the general case should be
clear.)

»  FNAME(arg1, plain(-X), arg3)
»    := -FNAME(arg1, X, arg3)
»
»  FNAME(arg1, plain(a X), arg3)
»    := a FNAME(arg1, X, arg3)   :: dreal(a) :: dscalar(a)
»
»  FNAME(arg1, plain(X a), arg3)
»    := FNAME(arg1, X, arg3) a   :: dreal(a) :: dscalar(a)
»
»  FNAME(arg1, plain(X / a), arg3)
»    := FNAME(arg1, X, arg3) / a :: dreal(a) :: dscalar(a)
»
»  FNAME(arg1, plain(X + Y), arg3)
»    := FNAME(arg1, X, arg3) + FNAME(arg1, Y, arg3)
»
»  FNAME(arg1, plain(X - Y), arg3)
»    := FNAME(arg1, X, arg3) - FNAME(arg1, Y, arg3)
"
  (let ((nn (or n 1))
        (jj (or j 1))
        (rule-name-ext
         (or rule-name
             (calctk-rule-name-symbol
              (calctk-lin-expand-name fname))))
        (arg1-symbol-ext (or arg1-symbol 'X))
        (arg2-symbol-ext (or arg2-symbol 'Y))
        (scalar-symbol-ext (or scalar-symbol 'a)))
    (let* ((var-form-a (calctk-var-form scalar-symbol-ext))
           (calcFunc-fname (calctk-calcFunc-name-symbol fname))
           (var-form-X (calctk-var-form arg1-symbol-ext))
           (var-form-Y (calctk-var-form arg2-symbol-ext)))
      `(calctk-def--rules
        ,rule-name-ext
        ;; expect(plain(-X)) := -expect(X) :: 1
        (calcFunc-condition
         (calcFunc-assign
          ,(calctk-gen-func-pattern nn jj
                                    calcFunc-fname
                                    `(calcFunc-plain (neg ,var-form-X)))
          (neg
           ,(calctk-gen-func-pattern nn jj
                                     calcFunc-fname
                                     var-form-X)))
         1)
        ;; expect(plain(a * X)) := a * expect(X) :: dreal(a) :: dscalar(a)
        (calcFunc-condition
         (calcFunc-condition
          (calcFunc-assign
           ,(calctk-gen-func-pattern nn jj
                                     calcFunc-fname
                                     `(calcFunc-plain
                                       (* ,var-form-a ,var-form-X)))
           (*
            ,var-form-a
            ,(calctk-gen-func-pattern nn jj
                                      calcFunc-fname
                                      var-form-X)))
          (calcFunc-dreal ,var-form-a))
         (calcFunc-dscalar ,var-form-a))
        ;; expect(plain(X * a)) := expect(X) * a :: dreal(a) :: dscalar(a)
        (calcFunc-condition
         (calcFunc-condition
          (calcFunc-assign
           ,(calctk-gen-func-pattern nn jj
                                     calcFunc-fname
                                     `(calcFunc-plain
                                       (* ,var-form-X ,var-form-a)))
           (*
            ,(calctk-gen-func-pattern nn jj
                                      calcFunc-fname
                                      var-form-X)
            ,var-form-a))
          (calcFunc-dreal ,var-form-a))
         (calcFunc-dscalar ,var-form-a))
        ;; expect(plain(X / a)) := expect(X) / a :: dreal(a) :: dscalar(a)
        (calcFunc-condition
         (calcFunc-condition
          (calcFunc-assign
           ,(calctk-gen-func-pattern nn jj
                                     calcFunc-fname
                                     `(calcFunc-plain
                                       (/ ,var-form-X ,var-form-a)))
           (/
            ,(calctk-gen-func-pattern nn jj
                                      calcFunc-fname
                                      var-form-X)
            ,var-form-a))
          (calcFunc-dreal ,var-form-a))
         (calcFunc-dscalar ,var-form-a))
        (calcFunc-condition
         (calcFunc-assign
          ,(calctk-gen-func-pattern nn jj
                                    calcFunc-fname
                                    `(calcFunc-plain
                                      (+ ,var-form-X ,var-form-Y)))
          (+
           ,(calctk-gen-func-pattern nn jj
                                     calcFunc-fname
                                     var-form-X)
           ,(calctk-gen-func-pattern nn jj
                                     calcFunc-fname
                                     var-form-Y)))
         1)
        (calcFunc-condition
         (calcFunc-assign
          ,(calctk-gen-func-pattern nn jj
                                    calcFunc-fname
                                    `(calcFunc-plain
                                      (- ,var-form-X ,var-form-Y)))
          (-
           ,(calctk-gen-func-pattern nn jj
                                     calcFunc-fname
                                     var-form-X)
           ,(calctk-gen-func-pattern nn jj
                                     calcFunc-fname
                                     var-form-Y)))
         1)))))

(defun calctk-add-to-Decls (expr declaration)
  "Declare «EXPR» as «DECLARATION» type.
Add the «[EXPR, DECLARATION]» construct to the «Decls»
variable. «EXPR» is usually a calc symbol representing a
variable, or a function call like this
«func(arg1, arg2)». «DECLARATION» is one of the symbols
influencing operations on «EXPR», specified in Info node
‘(calc)Kinds of Declarations’. For full documentation on
declarations see Info node ‘(calc)Declarations’.

Calc's analogue: \\<calc-mode-map>⟦\\[calc-declare-variable]⟧.

--- Arguments ---

| EXPR        | CalcExpr   |
| DECLARATION | CalcSymbol |
|-------------+------------|
| RESULT      | ExprForm   |
"
  (set (calctk-var-name-symbol 'Decls)
       (calctk-list-to-vec
        (-union (calctk-vec-to-list
                 (eval (calctk-var-name-symbol 'Decls)))
                (list (calctk-list-to-vec
                       (list (calctk-calc-expr expr)
                             (calctk-calc-expr declaration))))))))

;;;;
;;
;; Internals
;;
;;;;

(defun calctk-gen-default-args (n)
  "Return list of symbols ‹(arg1 arg2 … argN)› "
  (--map
   (intern (concat "arg" (number-to-string it)))
   (number-sequence 1 n)))

(defun calctk-gen-func-pattern (n j calcFunc-fname expr)
  "Generate «fname(a1, …, a_{J-1}, expr, a_{J+1}, …, aN)»."
  (cons calcFunc-fname
        (-replace-at
         (- j 1)
         expr
         (-map 'calctk-var-form
               (calctk-gen-default-args n)))))

(defun calctk-gen-alt-symbol (symbol)
  "Return ‘symbol231759’ or the like (SYMBOL + 6 random digits)"
  (concat (symbol-name symbol)
          (mapconcat (lambda (n)
                       (number-to-string (random n)))
                     (make-list 6 10)
                     "")))

;; cyclical-rules: CalcForm
;; opers: List<Symbol> (in algebraic form)

(defun calctk-guard-opers (cyclical-rules opers)
  (let ((calcFunc-opers-alt-names
         (-zip-pair
          (-map 'calctk-calcFunc-name-symbol opers)
          (-map 'calctk-calcFunc-name-symbol
                (-map 'calctk-gen-alt-symbol opers)))))
    (calctk-guard-opers-righthand-assign
     cyclical-rules
     calcFunc-opers-alt-names)))

(defun calctk-is-func (form func)
  "Return t if FORM calc sexp is an algebraic function «FUNC»."
  (calctk-is-cF-func
   form (calctk-calcFunc-name-symbol func)))

(defun calctk-is-cF-func (form cF-func)
  (and (listp form)
       (equal (car form) cF-func)))

(defun calctk-guard-opers-righthand-assign (form calcFunc-opers-alt-names)
  (cond ((calctk-is-func form 'assign)
         (cons (nth 0 form)
               (cons (nth 1 form)
                     (cons (calctk-rename-symbols
                            (nth 2 form)
                            calcFunc-opers-alt-names)
                           (cdddr form)))))
        ((listp form)
         (--map (calctk-guard-opers-righthand-assign
                 it
                 calcFunc-opers-alt-names)
                form))
        (t form)))

(defun calctk-rename-symbols (form symbols-assoc)
  "
| FORM          | CalcSexp   |
| SYMBOLS-ASSOC | List<Cons> |
"
  (cond ((listp form)
         (--map
          (calctk-rename-symbols it symbols-assoc)
          form))
        ((symbolp form)
         (or (cdr (assoc form symbols-assoc))
             form))
        (t form)))

(defun calctk-ensure-expression-sexp (form)
  "
| FORM   | CalcSexp |
|        | String   |
| RESULT | CalcSexp |
"
  (if (and form (stringp form))
      (car (math-read-exprs form))
    form))

;; varname: symbol

(defun calctk-var-form (varname)
  "
| VARNAME | CalcSymbol |
"
  (let ((var-varname (calctk-var-name
                      varname)))
    `(var ,varname ,var-varname)))


(defmacro calctk-element-builder (ELEMENT BUILDER &optional TEXT)
  "
--- Arguments ---

| ELEMENT | CalcSymbol      |
| BUILDER | String          |
|         | String → String |
"
  (let* ((element-str (symbol-name ELEMENT))
         (element-text-str (or TEXT element-str))
         (calctk-ELEMENT-str (intern
                               (concat "calctk-" element-str "-name-str")))
         (calctk-ELEMENT-symbol (intern
                                  (concat "calctk-" element-str "-name-symbol")))
         (calctk-ELEMENT (intern
                           (concat "calctk-" element-str "-name"))))
    `(progn

       ;; string → string

       (defun ,calctk-ELEMENT-str (base-name &rest rest)
         "
--- Arguments ---

| BASE-NAME | String  |
|           | Symbol  |
| REST      | String* |
|           | Symbol* |
|-----------+---------|
| RESULT    | String  |
"
         (apply
          #',BUILDER
          (calctk-ensure-string base-name)
          (-map 'calctk-ensure-string rest)))

       (defun ,calctk-ELEMENT-symbol (base-name &rest rest)
         "
--- Arguments ---

| BASE-NAME | String  |
|           | Symbol  |
| REST      | String* |
|           | Symbol* |
|-----------+---------|
| RESULT    | Symbol  |
"
         (intern (apply
                  ',calctk-ELEMENT-str
                  base-name
                  rest)))

       ;; T → T
       ;; T = string | symbol

       (defun ,calctk-ELEMENT (base-name &rest rest)
         "
--- Arguments ---

| BASE-NAME | String            |
|           | Symbol            |
| REST      | String*           |
|           | Symbol*           |
|-----------+-------------------|
| RESULT    | typeof(BASE-NAME) |
"
         (if (stringp base-name)
             (apply
              ',calctk-ELEMENT-str
              base-name
              rest)
           (intern (apply
                    ',calctk-ELEMENT-str
                    base-name
                    rest)))))))


(defun calctk-camel-joiner (text &rest rest)
  "Join strings in TEXT and REST “inThisFashion”."
  (concat
   text
   (mapconcat
    'calctk-upcase-first-letter
    rest
    "")))

;; | Function               | Operands                 | Result                      |
;; |------------------------+--------------------------+-----------------------------|
;; | calctk-var-name        | vname                    | var-vname                   |
;; | calctk-calc-name       | fname                    | calc-fname                  |
;; | calctk-calcFunc-name   | fname                    | calcFunc-fname              |
;; | calctk-reduce-name     | name                     | reduceName                  |
;; | calctk-rule-name       | rname                    | ruleRname                   |
;; | calctk-builder-name    | fname                    | bfname                      |
;; | calctk-assoc-name      | opname left              | opnameLeftAssocNorm         |
;; | calctk-conversion-name | firstop secondop         | firstopToSecondop           |
;; | calctk-assocComb-name  | firstop secondop thirdop | assocFirstopSecondopThirdop |
;; | calctk-lin-expand-name | oper                     | operLinExpand               |
;; #+TBLFM: $3='(apply (intern $1) (s-split-words $2))

;; calctk-var-name-str
;; calctk-var-name
;; calctk-var-name-symbol
(calctk-element-builder
 var (lambda (str)
       (concat "var-" str)))

;; calctk-calc-name-str
;; calctk-calc-name
;; calctk-calc-name-symbol
(calctk-element-builder
 calc (lambda (str)
        (concat "calc-" str)))

;; calctk-calcFunc-name-str
;; calctk-calcFunc-name
;; calctk-calcFunc-name-symbol
(calctk-element-builder
 calcFunc (lambda (str)
            (concat "calcFunc-" str)))

;; calctk-reduce-name-str
;; calctk-reduce-name
;; calctk-reduce-name-symbol
(calctk-element-builder
 reduce (lambda (str)
          (calctk-camel-joiner "reduce" str)))

;; calctk-rule-name-str
;; calctk-rule-name
;; calctk-rule-name-symbol
(calctk-element-builder
 rule (lambda (str)
        (calctk-camel-joiner "rule" str)))

;; calctk-builder-name-str
;; calctk-builder-name
;; calctk-builder-name-symbol
(calctk-element-builder
 builder (lambda (str)
           (concat "b" str)))

;; calctk-assoc-name-str
;; calctk-assoc-name
;; calctk-assoc-name-symbol
(calctk-element-builder
 assoc (lambda (str left-assoc)
         (calctk-camel-joiner str
                              left-assoc
                              "assocNorm")))

;; calctk-conversion-name-str
;; calctk-conversion-name
;; calctk-conversion-name-symbol
(calctk-element-builder
 conversion (lambda (left right)
              (calctk-camel-joiner left "to" right)))

;; calctk-assocComb-name-str
;; calctk-assocComb-name
;; calctk-assocComb-name-symbol
(calctk-element-builder
 assocComb (lambda (oper &rest opers)
             (apply
              'calctk-camel-joiner
              "assoc"
              oper
              opers)))

;; calctk-lin-expand-name-str
;; calctk-lin-expand-name
;; calctk-lin-expand-name-symbol
(calctk-element-builder
 lin-expand (lambda (str)
              (concat str "LinExpand")))


;; Take first 4 letters
(defun calctk-trail-name (func-name-str)
  (substring func-name-str 0 (min 4 (length func-name-str))))

(defun calctk-upcase-first-letter (str)
  (if (> (length str) 0)
       (concat (upcase (substring str 0 1))
               (substring str 1))
    ""))

(defun calctk-downcase-first-letter (str)
  (if (> (length str) 0)
      (concat (downcase (substring str 0 1))
               (substring str 1))
    ""))

(defun calctk-commafy-form (args form)
  "Make all variables in FORM occuring in ARGS a macro placeholder.

--- Arguments ---

| ARGS | List<CalcSymbol> |
| FORM | CalcSexp         |

Replace in FORM sexp all instances of

›  (var var-X X)

for some symbol X occuring in ARGS list with

›  ,X

--- Example ---

»        pi t
»  A exp(----)
»         2

›  (calctk-commafy-form
›   '(t)
›   '(* (var A var-A)
›       (calcFunc-exp
›        (/ (* (var pi var-pi)
›              (var t var-t))
›           2))))

=>

›  (* (var A var-A)
›     (calcFunc-exp
›      (/ (* (var pi var-pi)
›            (\, t))
›         2)))
"
  (if (listp form)
      (if (equal (car form) 'var)
          (if (memq (cadr form) args)
              (list '\, (cadr form))
            form)
        (-map
         (lambda (x)
           (calctk-commafy-form args x))
         form))
    form))


;; TOREMOVE
(defun calctk-toVec (oper-from)
  "
| OPER-FROM | String |
|-----------+--------|
| RESULT    | String |
"
  (calctk-conversion-name-str oper-from "vec"))

(defun calctk-list-to-vec (lst)
  (append '(vec) lst))

(defun calctk-sexp-to-vec (sexp)
  (if (and (listp sexp)
           (or (= (length sexp) 0)
               (not (memq (car sexp) '(var)))))
      (calctk-list-to-vec
       (-map 'calctk-sexp-to-vec sexp))
    sexp))

(defun calctk-vec-to-list (vec)
  (cdr vec))

(defun calctk-elem-is-vec (elem)
  (and (listp elem)
       (> (length elem) 0)
       (equal (car elem) 'vec)))

(defun calctk-elem-is-func (elem func)
  "
| ELEM | CalcForm   |
| FUNC | CalcSymbol |
|      | String     |
"
  (and (listp elem)
       (> (length elem) 0)
       (equal (car elem) (calctk-calcFunc-name-symbol func))))


(defun calctk-calc-expr (expr)
  "Convert EXPR into form recognized by calc

| EXPR   | String   |
|        | CalcSexp |
|--------+----------|
| RESULT | CalcSexp |
"
  (if (stringp expr)
      (calctk-safe-read-expr expr)
    expr))

(defun calctk-safe-read-expr (expr)
  "
Arguments:
| EXPR   | String   |
|--------+----------|
| RESULT | CalcForm |
"
  (let ((current-mode calc-simplify-mode))
    (call-interactively 'calc-no-simplify-mode)
    (let ((parsed-expr (math-read-expr expr)))
      (calc-wrapper
       (calc-set-simplify-mode current-mode nil ""))
      parsed-expr)))

(provide 'calctk)
